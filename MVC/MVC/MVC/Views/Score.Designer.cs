﻿namespace MVC.Views
{
    partial class Score
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearchYear = new System.Windows.Forms.Button();
            this.cmbPickHalf = new System.Windows.Forms.ComboBox();
            this.cmbPickYear = new System.Windows.Forms.ComboBox();
            this.cmbPickClass = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSearchStudent = new System.Windows.Forms.Button();
            this.cmbIdStudent = new System.Windows.Forms.ComboBox();
            this.txtNameStudent = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpClass = new System.Windows.Forms.Button();
            this.btnUpdateScore = new System.Windows.Forms.Button();
            this.btnAddScore = new System.Windows.Forms.Button();
            this.dgrScoreStudent = new System.Windows.Forms.DataGridView();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrScoreStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnSearchYear);
            this.groupBox2.Controls.Add(this.cmbPickHalf);
            this.groupBox2.Controls.Add(this.cmbPickYear);
            this.groupBox2.Controls.Add(this.cmbPickClass);
            this.groupBox2.Location = new System.Drawing.Point(56, 22);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1003, 72);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(438, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Học kì:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Năm:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Chọn lớp:";
            // 
            // btnSearchYear
            // 
            this.btnSearchYear.Location = new System.Drawing.Point(618, 33);
            this.btnSearchYear.Name = "btnSearchYear";
            this.btnSearchYear.Size = new System.Drawing.Size(75, 23);
            this.btnSearchYear.TabIndex = 3;
            this.btnSearchYear.Text = "Tìm";
            this.btnSearchYear.UseVisualStyleBackColor = true;
            this.btnSearchYear.Click += new System.EventHandler(this.btnSearchYear_Click);
            // 
            // cmbPickHalf
            // 
            this.cmbPickHalf.FormattingEnabled = true;
            this.cmbPickHalf.Items.AddRange(new object[] {
            "Kì I",
            "Kì II"});
            this.cmbPickHalf.Location = new System.Drawing.Point(508, 33);
            this.cmbPickHalf.Name = "cmbPickHalf";
            this.cmbPickHalf.Size = new System.Drawing.Size(76, 24);
            this.cmbPickHalf.TabIndex = 2;
            // 
            // cmbPickYear
            // 
            this.cmbPickYear.FormattingEnabled = true;
            this.cmbPickYear.Location = new System.Drawing.Point(293, 33);
            this.cmbPickYear.Name = "cmbPickYear";
            this.cmbPickYear.Size = new System.Drawing.Size(121, 24);
            this.cmbPickYear.TabIndex = 1;
            // 
            // cmbPickClass
            // 
            this.cmbPickClass.FormattingEnabled = true;
            this.cmbPickClass.Location = new System.Drawing.Point(80, 34);
            this.cmbPickClass.Name = "cmbPickClass";
            this.cmbPickClass.Size = new System.Drawing.Size(121, 24);
            this.cmbPickClass.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.btnSearchStudent);
            this.groupBox3.Controls.Add(this.cmbIdStudent);
            this.groupBox3.Controls.Add(this.txtNameStudent);
            this.groupBox3.Location = new System.Drawing.Point(56, 103);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1003, 61);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(227, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Mã sinh viên";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Tên";
            // 
            // btnSearchStudent
            // 
            this.btnSearchStudent.Location = new System.Drawing.Point(515, 19);
            this.btnSearchStudent.Name = "btnSearchStudent";
            this.btnSearchStudent.Size = new System.Drawing.Size(75, 23);
            this.btnSearchStudent.TabIndex = 2;
            this.btnSearchStudent.Text = "Tìm";
            this.btnSearchStudent.UseVisualStyleBackColor = true;
            // 
            // cmbIdStudent
            // 
            this.cmbIdStudent.FormattingEnabled = true;
            this.cmbIdStudent.Location = new System.Drawing.Point(320, 19);
            this.cmbIdStudent.Name = "cmbIdStudent";
            this.cmbIdStudent.Size = new System.Drawing.Size(121, 24);
            this.cmbIdStudent.TabIndex = 1;
            // 
            // txtNameStudent
            // 
            this.txtNameStudent.Location = new System.Drawing.Point(80, 19);
            this.txtNameStudent.Name = "txtNameStudent";
            this.txtNameStudent.Size = new System.Drawing.Size(121, 22);
            this.txtNameStudent.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnSave);
            this.groupBox4.Controls.Add(this.btnUpClass);
            this.groupBox4.Controls.Add(this.btnUpdateScore);
            this.groupBox4.Controls.Add(this.btnAddScore);
            this.groupBox4.Controls.Add(this.dgrScoreStudent);
            this.groupBox4.Location = new System.Drawing.Point(31, 179);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1028, 357);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(271, 21);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Luu";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpClass
            // 
            this.btnUpClass.Location = new System.Drawing.Point(382, 21);
            this.btnUpClass.Name = "btnUpClass";
            this.btnUpClass.Size = new System.Drawing.Size(75, 23);
            this.btnUpClass.TabIndex = 3;
            this.btnUpClass.Text = "Lên lớp";
            this.btnUpClass.UseVisualStyleBackColor = true;
            this.btnUpClass.Click += new System.EventHandler(this.btnUpClass_Click);
            // 
            // btnUpdateScore
            // 
            this.btnUpdateScore.Location = new System.Drawing.Point(157, 21);
            this.btnUpdateScore.Name = "btnUpdateScore";
            this.btnUpdateScore.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateScore.TabIndex = 2;
            this.btnUpdateScore.Text = "Sửa";
            this.btnUpdateScore.UseVisualStyleBackColor = true;
            this.btnUpdateScore.Click += new System.EventHandler(this.btnUpdateScore_Click);
            // 
            // btnAddScore
            // 
            this.btnAddScore.Location = new System.Drawing.Point(50, 21);
            this.btnAddScore.Name = "btnAddScore";
            this.btnAddScore.Size = new System.Drawing.Size(75, 23);
            this.btnAddScore.TabIndex = 1;
            this.btnAddScore.Text = "Thêm";
            this.btnAddScore.UseVisualStyleBackColor = true;
            this.btnAddScore.Visible = false;
            this.btnAddScore.Click += new System.EventHandler(this.btnAddScore_Click);
            // 
            // dgrScoreStudent
            // 
            this.dgrScoreStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrScoreStudent.Location = new System.Drawing.Point(6, 51);
            this.dgrScoreStudent.Name = "dgrScoreStudent";
            this.dgrScoreStudent.RowTemplate.Height = 24;
            this.dgrScoreStudent.Size = new System.Drawing.Size(1022, 300);
            this.dgrScoreStudent.TabIndex = 0;
            this.dgrScoreStudent.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgrScoreStudent_DataError);
            // 
            // Score
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 604);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Name = "Score";
            this.Text = "Score";
            this.Load += new System.EventHandler(this.Score_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrScoreStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSearchYear;
        private System.Windows.Forms.ComboBox cmbPickHalf;
        private System.Windows.Forms.ComboBox cmbPickYear;
        private System.Windows.Forms.ComboBox cmbPickClass;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSearchStudent;
        private System.Windows.Forms.ComboBox cmbIdStudent;
        private System.Windows.Forms.TextBox txtNameStudent;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgrScoreStudent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnUpdateScore;
        private System.Windows.Forms.Button btnAddScore;
        private System.Windows.Forms.Button btnUpClass;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button btnSave;
    }
}