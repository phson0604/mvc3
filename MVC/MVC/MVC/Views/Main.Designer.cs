﻿namespace MVC.Views
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.TeacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MNTeacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnScoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChairmanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TeacherToolStripMenuItem,
            this.StudentToolStripMenuItem,
            this.scoreToolStripMenuItem,
            this.ChairmanToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(900, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // TeacherToolStripMenuItem
            // 
            this.TeacherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MNTeacherToolStripMenuItem});
            this.TeacherToolStripMenuItem.Name = "TeacherToolStripMenuItem";
            this.TeacherToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.TeacherToolStripMenuItem.Text = "Giáo viên";
            // 
            // MNTeacherToolStripMenuItem
            // 
            this.MNTeacherToolStripMenuItem.Name = "MNTeacherToolStripMenuItem";
            this.MNTeacherToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.MNTeacherToolStripMenuItem.Text = "Quản lí giáo viên";
            this.MNTeacherToolStripMenuItem.Click += new System.EventHandler(this.MNTeacherToolStripMenuItem_Click);
            // 
            // StudentToolStripMenuItem
            // 
            this.StudentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnStudentToolStripMenuItem});
            this.StudentToolStripMenuItem.Name = "StudentToolStripMenuItem";
            this.StudentToolStripMenuItem.Size = new System.Drawing.Size(78, 24);
            this.StudentToolStripMenuItem.Text = "Học sinh";
            // 
            // mnStudentToolStripMenuItem
            // 
            this.mnStudentToolStripMenuItem.Name = "mnStudentToolStripMenuItem";
            this.mnStudentToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.mnStudentToolStripMenuItem.Text = "Quản lí học sinh";
            this.mnStudentToolStripMenuItem.Click += new System.EventHandler(this.mnStudentToolStripMenuItem_Click);
            // 
            // scoreToolStripMenuItem
            // 
            this.scoreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnScoreToolStripMenuItem});
            this.scoreToolStripMenuItem.Name = "scoreToolStripMenuItem";
            this.scoreToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.scoreToolStripMenuItem.Text = "Điểm";
            this.scoreToolStripMenuItem.Click += new System.EventHandler(this.scoreToolStripMenuItem_Click);
            // 
            // mnScoreToolStripMenuItem
            // 
            this.mnScoreToolStripMenuItem.Name = "mnScoreToolStripMenuItem";
            this.mnScoreToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.mnScoreToolStripMenuItem.Text = "Quản lí điểm";
            this.mnScoreToolStripMenuItem.Click += new System.EventHandler(this.mnScoreToolStripMenuItem_Click);
            // 
            // ChairmanToolStripMenuItem
            // 
            this.ChairmanToolStripMenuItem.Name = "ChairmanToolStripMenuItem";
            this.ChairmanToolStripMenuItem.Size = new System.Drawing.Size(94, 24);
            this.ChairmanToolStripMenuItem.Text = "Chủ Nhiệm";
            this.ChairmanToolStripMenuItem.Click += new System.EventHandler(this.ChairmanToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(59, 24);
            this.exitToolStripMenuItem.Text = "Thoát";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click_1);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 425);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Main";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem TeacherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MNTeacherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnScoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChairmanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}