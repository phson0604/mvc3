﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVC.Models;
using MVC.Controllers;


namespace MVC.Views
{
    public partial class Score : Form
    {
        Ctrl_Score ctrl = new Ctrl_Score();
         
        public Score()
        {
            InitializeComponent();
        }

        private void Score_Load(object sender, EventArgs e)
        {
            ctrl.cmb_class(cmbPickClass);
            ctrl.cmb_year(cmbPickYear);         
        }

        //Search year and class
        private void btnSearchYear_Click(object sender, EventArgs e)
        {
            ctrl.select_students(dgrScoreStudent, cmbPickClass.Text, cmbPickYear.Text,cmbPickHalf.Text ,cmbIdStudent);
            bindingSource1.DataSource = dgrScoreStudent.DataSource;
            //dgrScoreStudent.Columns.Remove("Class");
            //dgrScoreStudent.Columns.Remove("Student");
        }
    
       
        int add = 0;
        private void btnAddScore_Click(object sender, EventArgs e)
        {
           
            //ctrl.add_score_to_class(bindingSource1, dgrScoreStudent);
            
            //if (add == 1)
            //{
               
            //    ctrl.cre_score_info(dgrScoreStudent);
            //    //dgrScoreStudent.Columns.Remove("Class");
            //    //dgrScoreStudent.Columns.Remove("Student");
            //    add = 2;
            //}
            //if (add == 0)
            //{
            //    add = 1;
            //}
            //if (add == 2)
            //{
            //    add = 0;
            //}
        }

        private void dgrScoreStudent_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        int update = 0;
        private void btnUpdateScore_Click(object sender, EventArgs e)
        {
            update = 1;
        }

        //Save update
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (update == 1)
            {
                ctrl.up_score_stu_info(dgrScoreStudent);
                update = 0;
            }
        }

        //Update score student
        private void btnUpClass_Click(object sender, EventArgs e)
        {
            ctrl.substring(cmbPickYear, cmbPickClass,dgrScoreStudent.CurrentRow.Cells["IDStudent"].Value.ToString());         
        }
    }
}
