namespace MVC.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Windows.Forms;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Chairnam> Chairnams { get; set; }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<LearningPower> LearningPowers { get; set; }
        public virtual DbSet<Score> Scores { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<YearClass> YearClasses { get; set; }
        public virtual DbSet<C_tc> C_tc { get; set; }
        public virtual DbSet<InputScore> InputScores { get; set; }
        public virtual DbSet<LsClass> LsClasses { get; set; }
        public virtual DbSet<V_Score> V_Score { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.Pass)
                .IsUnicode(false);

            modelBuilder.Entity<Chairnam>()
                .Property(e => e.IdCm)
                .IsUnicode(false);

            modelBuilder.Entity<Chairnam>()
                .Property(e => e.IdTc)
                .IsUnicode(false);

            modelBuilder.Entity<Chairnam>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<Class>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<Score>()
                .Property(e => e.IDScore)
                .IsUnicode(false);

            modelBuilder.Entity<Score>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<Score>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<Student>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<Teacher>()
                .Property(e => e.IdTc)
                .IsUnicode(false);

            modelBuilder.Entity<Teacher>()
                .Property(e => e.PhoneTC)
                .IsUnicode(false);

            modelBuilder.Entity<C_tc>()
                .Property(e => e.IdTc)
                .IsUnicode(false);

            modelBuilder.Entity<C_tc>()
                .Property(e => e.PhoneTC)
                .IsUnicode(false);

            modelBuilder.Entity<InputScore>()
                .Property(e => e.IDScore)
                .IsUnicode(false);

            modelBuilder.Entity<InputScore>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<InputScore>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<LsClass>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<LsClass>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<V_Score>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<V_Score>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);
        }
        public static void Select_GV(DataGridView dgv)
        {
            using (Context ct = new Context())
            {
                dgv.DataSource = ct.C_tc.ToList();
            }
        }
        public static void insert_GV(Teacher gv)
        {
            using (Context ct = new Context())
            {
                ct.Teachers.Add(gv);
                ct.SaveChanges();
            }
        }
        public static void update_GV(Context ct)
        {
            ct.SaveChanges();
        }
        public static void del_teacher(Context ct)
        {
            ct.SaveChanges();
        }
        public static void un_del_teacher(Context ct)
        {
            ct.SaveChanges();
        }
        public static void cbb_classes(ComboBox x)
        {
            using (Context ct = new Context())
            {
                x.DataSource = ct.Classes.ToList();
                x.DisplayMember = "IdClass";
                x.ValueMember = "IdClass";
            }
        }
        public static void Select_students(DataGridView dgv, string clas, string year, ComboBox id_stu)
        {
            using (Context ct = new Context())
            {
                dgv.DataSource = ct.LsClasses.Where(w => w.IdClass == clas && w.YearClass == year).ToList();
                id_stu.DataSource = ct.LsClasses.Where(w => w.IdClass == clas && w.YearClass == year).ToList();
                id_stu.DisplayMember = "IDStudent";
                id_stu.ValueMember = "IDStudent";
            }
        }
        public static void add_stu_to_sql(Context ct, Student info, StudentClass a, int add)
        {
            if (add == 1)
            {
                ct.Students.Add(info);
                ct.StudentClasses.Add(a);
                ct.SaveChanges();
            }
        }
        public static void add_stu_to_sql(Context ct)
        {
            ct.SaveChanges();
        }
        public static void del_stu(DataGridView x)
        {
            using (Context ct = new Context())
            {
                string classid = "", stuid = "";
                classid = x.CurrentRow.Cells["IdClass"].Value.ToString();
                stuid = x.CurrentRow.Cells["IDStudent"].Value.ToString();
                object stu = ct.StudentClasses.Where(w => w.IDStudent == stuid && w.IdClass == classid).FirstOrDefault();
                ct.Entry(stu).State = EntityState.Deleted;
                ct.SaveChanges();
            }
        }

    }
}
