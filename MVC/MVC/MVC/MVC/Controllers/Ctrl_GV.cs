﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVC.Models;

namespace MVC.Controllers
{
    class Ctrl_GV
    {
        public void btn_change_status(Control x)
        {
            x.Enabled = !x.Enabled;
        }
        public void _insert_teacher(string id, string name, string gender, DateTime dob, string address, string phone)
        {
            string notice = "Còn thiếu:";
            #region condition_checking
            using (Context ct = new Context())
            {
                if (ct.Teachers.Where(w => w.IdTc == id).ToList().Count > 0)
                {
                    MessageBox.Show("Trùng mã");
                    return;
                }
            }
            if (string.IsNullOrEmpty(id))
            {
                notice += "\t\nMã GV";
            }
            if (string.IsNullOrEmpty(name))
            {
                notice += "\t\nTên GV";
            }
            if (string.IsNullOrEmpty(gender))
            {
                notice += "\t\nGiới tính";
            }
            if (string.IsNullOrEmpty(dob.ToShortDateString()))
            {
                notice += "\t\nNgày sinh";
            }
            if (string.IsNullOrEmpty(address))
            {
                notice += "\t\nĐịa chỉ";
            }
            if (string.IsNullOrEmpty(phone))
            {
                notice += "\t\nSĐT";
            }
#endregion
            if(notice!= "Còn thiếu:")
            {
                MessageBox.Show(notice, "Nhắc", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                Form1.error = 1;
                return;
            }
            Teacher gv = new Teacher()
            {
                IdTc = id,
               TeacherName = name,
                Sex = gender,
                TeacherDate=dob,
                AdressTC=address,
                PhoneTC=phone
            };
            Context.insert_GV(gv);
        }
        public void _update_teacher(string id, string name, string gender, DateTime dob, string address, string phone)
        {
            string notice = "Còn thiếu:";
            #region condition_checking
            //using (Context ct = new Context())
            //{
            //    if (ct.GiaoViens.Where(w => w.MaGV == id).ToList().Count > 0)
            //    {
            //        MessageBox.Show("Trùng mã");
            //        return;
            //    }
            //}
            if (string.IsNullOrEmpty(id))
            {
                notice += "\t\nMã GV";
            }
            if (string.IsNullOrEmpty(name))
            {
                notice += "\t\nTên GV";
            }
            if (string.IsNullOrEmpty(gender))
            {
                notice += "\t\nGiới tính";
            }
            if (string.IsNullOrEmpty(dob.ToShortDateString()))
            {
                notice += "\t\nNgày sinh";
            }
            if (string.IsNullOrEmpty(address))
            {
                notice += "\t\nĐịa chỉ";
            }
            if (string.IsNullOrEmpty(phone))
            {
                notice += "\t\nSĐT";
            }
            #endregion
            if (notice != "Còn thiếu:")
            {
                MessageBox.Show(notice, "Nhắc", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                Form1.error = 1;
                return;
            }
            Teacher gv = new Teacher();

            using (Context ct = new Context())
            {
                gv = ct.Teachers.Where(w => w.IdTc == id).FirstOrDefault();
                gv.TeacherName = name;
                gv.TeacherDate = dob;
                gv.Sex = gender;
                gv.AdressTC = address;
                gv.PhoneTC = phone;
                Context.update_GV(ct);
            }
        }
        public void _del_teacher(Context ct, Teacher gv)
        {

            ct.Entry(gv).State = EntityState.Deleted;
            Context.del_teacher(ct);
        }
        public void un_del(List<Teacher> l)
        {
            using (Context ct = new Context())
            {
                ct.Teachers.AddRange(l);
                Context.un_del_teacher(ct);
            }
        }
    }
}
