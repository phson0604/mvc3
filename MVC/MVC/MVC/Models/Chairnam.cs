namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Chairnam")]
    public partial class Chairnam
    {
        [Key]
        [StringLength(10)]
        public string IdCm { get; set; }

        [StringLength(20)]
        public string IdTc { get; set; }

        [StringLength(10)]
        public string IdClass { get; set; }

        [StringLength(20)]
        public string YearClass { get; set; }

        public virtual Class Class { get; set; }

        public virtual Teacher Teacher { get; set; }
    }
}
