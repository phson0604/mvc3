namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StudentClass")]
    public partial class StudentClass
    {
        [Key]
        public int IdStudentClass { get; set; }

        [StringLength(10)]
        public string IDStudent { get; set; }

        [StringLength(10)]
        public string IdClass { get; set; }

        [StringLength(20)]
        public string YearClass { get; set; }

        public virtual Class Class { get; set; }

        public virtual Student Student { get; set; }
    }
}
