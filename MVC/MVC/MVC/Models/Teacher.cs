namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Teacher")]
    public partial class Teacher
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Teacher()
        {
            Chairnams = new HashSet<Chairnam>();
        }

        [Key]
        [StringLength(20)]
        public string IdTc { get; set; }

        [StringLength(50)]
        public string TeacherName { get; set; }

        [StringLength(25)]
        public string Sex { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TeacherDate { get; set; }

        [StringLength(50)]
        public string AdressTC { get; set; }

        [StringLength(20)]
        public string PhoneTC { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Chairnam> Chairnams { get; set; }
    }
}
