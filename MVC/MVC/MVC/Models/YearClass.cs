namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("YearClass")]
    public partial class YearClass
    {
        [Key]
        [StringLength(20)]
        public string nam { get; set; }
    }
}
